#!/bin/bash

echo "starting..."

source ./variables.sh

usage()
{
  echo "Usage: ./get-cluster-configs.sh all|token"
  exit 1
}

CONFIG_FOLDER="kubeconfigs"

function get_tokens() {
    for dir in ./${CONFIG_FOLDER}/*; do
        DIR="$dir"
        CLUSTER="${DIR##./*/}"

        TOKEN=$(get_login_token $CLUSTER)
        if [[ -z $TOKEN ]]; then
            echo "TOKEN missing for ${CLUSTER}"
        else
            echo "${CLUSTER}:"
            echo ""
            echo $TOKEN
            echo ""
        fi
    done
}

function get_remote_configs() {

    if [[ -d ./kubeconfigs ]]; then
        rm -rf ./kubeconfigs
    fi

    echo -e "\n\nPulling configs for GCP Project [${PROJECT_ID}]\n\n"
    # Downloading all configs
    echo "Remote kubectonfigs do not exist, downloading to local machine..."
    gsutil -m cp -r gs://${BUCKET_NAME}/${CONFIG_FOLDER} .

    for dir in ./${CONFIG_FOLDER}/*; do
        DIR="$dir"
        CLUSTER="${DIR##./*/}"
        echo "Setting up KUBECONFIG for '${CLUSTER}'"
        IP=$(gcloud compute instances list --filter="name=$CLUSTER" --format="value(networkInterfaces.accessConfigs.natIP)")
        IP2="${IP//[\'/}"
        FINAL_IP="${IP2//\']/}"

        if [ -z ${FINAL_IP} ]; then
            echo "ERROR: Cannot determine IP address of GCE instance containing cluster [${CLUSTER}]"
            exit 1
        fi
        echo "Final IP: ${FINAL_IP}"
        sed -i "s/127.0.0.1/$FINAL_IP/g" ${dir}/k3s.yaml

        echo "export KUBECONFIG=./k3s.yaml" > $dir/.envrc
    done
    echo -e "NOTE: KubeAPI/kubectl TLS is not enabled for this demo, please use:\n    kubectl --insecure-skip-tls-verify cluster-info\n\n"
}

if [[ $1 == "all" ]] || [[ -z "$1" ]]; then
    get_remote_configs
    get_tokens
elif [[ $1 == "token" ]]; then
    get_tokens
else
    echo "Input not recognized"
    usage
    exit 1
fi
