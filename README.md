# Overview

This repository will create __X__ number of `k3s` single-node clusters inside of a `gce` (Google Compute Engine) VM.  Additionally, the `k3s` clusters will be configured to use a git-repo for configuration using Anthos Config Management.

## Known issues

When creating more than 15 clusters, GKE Hub and GKE Connect will violate default quotas. These need to be increased via the cloud console. Additionally, each zone can have at most 72 CPUs in use. The default machine is set to be a 2-core, so at most 36 clusters can be created (if a completely empty project)

• compute.googleapis.com/cpus - Zone - cpu - Default:Varied by zone (7-day and concurrent)
• gkeconnect.googleapis.com/connect_requests - Global - Default:15 (7-day & concurrent)
• gkehub.googleapis.com/global-per-project-memberships - Global - Default:15 (7-day & concurrent)

## Setup
```bash
./install.sh -c <number instances>
```

## Logging in (Cloud Console)
Cloud console requires a "token" from a ServiceAccount inside of Kubernetes in order to view the state of the K8s API.  Run the following from each cluster to obtain the token:

### Automated (logging in)
During the `get-cluster-configs.sh` script, all clusters' OAuth tokens are printed. You can manually re-run just the token output by adding `token` as the first parameter:

```bash
./get-cluster-configs.sh token
```

### Manual Method (logging in)
```bash
export SECRET_NAME=$(kubectl get serviceaccount console-cluster-reader -o jsonpath='{$.secrets[0].name}')
export TOKEN=$(kubectl get secret ${SECRET_NAME} -o jsonpath='{$.data.token}' | base64 --decode)

echo $TOKEN
```

## Adding Secrets for Git Repo

By default, there are no secrets to access any of the Namespace repos. During installation, the `RootRepo` will use the environment-variables for GitLab Personal Access Token.  For the namespaces,
once cluster is up:
1. Generate the KUBECONFIG files using `./get-cluster-configs.sh`
1. Change directory into the `/kubeconfig/k3s-X` folder
1. Run the `.envrc` file (either using `direnv` app or `source .envrc`)
1. Run `kubectl --insecure-skip-tls-verify cluster-info` to allow `kubectl` to run without verifying TLS (see notes below as to why)
1. Run the following: `kubectl create secret generic git-creds --from-literal="username=${GITLAB_TOKEN_REPO_USER}" --from-literal="token=${GITLAB_TOKEN_REPO_SYNC}" --namespace customer-X` __#NOTE: X needs to be the cluster/folder for the namespace__
    1. The above assumes that the environment variables: `GITLAB_TOKEN_REPO_USER` and `GITLAB_TOKEN_REPO_SYNC` are still in-scope. If not, please set these to the User and Token respectively
1. Namespace repository will start syncing upon the creation of the `git-creds` Secret

### View Setup Logs
```bash
source ./variables.sh # setup the variables
export INSTANCE_NUMBER="1"
# TODO: Too clever splitting across zones, need to find Zone->GCE instance
gcloud beta compute ssh --zone "${ZONE}" "k3s-${INSTANCE_NUMBER}" --project "${PROJECT_ID}"
sudo journalctl -u google-startup-scripts.service
```

## Remove/Reset

```bash
./remove.sh
```

# Resources Created

## Bucket
GCS Bucket storing generated and shared configuration for bootstrapping k3s

Format as follows:

```bash
k3s-bootstrapping-${PROJECT_ID}
```

### Structure

* Cluster `kubeconfig` files for each cluster inside of bucket
    ```bash
    gs://k3s-bootstrapping-${PROJECT_ID}/kubeconfigs/k3s-[i]/*.yaml
    ```


### Using Kubectl


```bash
#Assuming downloaded config file as "k3s-1-config.yaml"
export KUBECONFIG=./k3s-1-config.yaml

# Important to include insecure-skip-tls-verify, certs are for 10.x.x.x only
kubectl --insecure-skip-tls-verify cluster-info
```
