#!/bin/bash

########################################################################
########################################################################
###### User specific variables (defaults are in repo) ##################
########################################################################
########################################################################
export MACHINE_SIZE="e2-highcpu-4" # replicating Coral Dev Board
export GITLAB_SECRET_TOKEN=${GITLAB_SECRET_TOKEN:-"PLEASE_SET"} # local project-based, read-only token
export GITLAB_SECRET_USER=${GITLAB_SECRET_USER:-"k3s-demo"}
export ROOT_REPO="https://gitlab.com/gcp-sa-iot-team/anthos-consumer-edge/acm-multi-root-demo.git"
export CONFIG_SYNC_VERSION=${CONFIG_SYNC_VERSION:-latest}
export K3S_VERSION="${K3S_VERSION:-}" # default is empty for "latest"

########################################################################
########################################################################
###### GENERAL VARIABLES -- unlikely needing to change ######
########################################################################
########################################################################
export PROJECT_ID=${PROJECT_ID:-$(gcloud config list --format 'value(core.project)')}
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=${REGION:-$(gcloud config get-value compute/region 2> /dev/null)}
export ZONE=${ZONE:-$(gcloud config get-value compute/zone 2> /dev/null)}
export AVAILABLE_ZONES=($(gcloud compute zones list --filter="region=${REGION}" --format="value(name)" | xargs | sed -e 's/\n/ /g'))
export ACCOUNT_EMAIL=${ACCOUNT_EMAIL:-$(gcloud auth list --filter=status:ACTIVE --format="value(account)")}
export USERNAME=$(echo "${ACCOUNT_EMAIL}" | cut -d@ -f1)
export BUCKET_NAME="k3s-bootstrapping-${PROJECT_ID}"
export HUB_SA="gke-hub-gsa" # Service Account for GKE Hub

# If starting index was specified at script runtime, use that number
if [[ ! -z "${STARTING_INDEX}" ]]; then
    export CLUSTER_START_INDEX=${STARTING_INDEX}
else
    export CLUSTER_START_INDEX=1
fi

########################################################################
########################################################################


# Create a secret and version; if secret already exists, adds a version. Idempotent(ish)
function create_secret() {
    KEY="$1"
    VALUE="$2"
    FILE="${3-false}"
    gcloud secrets -q versions access latest --secret="${KEY}" 2> /dev/null
    if [[ $? > 0 ]]; then
        gcloud secrets create ${KEY} --replication-policy="automatic"
    fi
    if [[ "$FILE" == "false" ]]; then
        # Standard Input
        echo ${VALUE} | gcloud secrets versions add ${KEY} --data-file=-
    else
        # File reference
        gcloud secrets versions add ${KEY} --data-file=${VALUE}
    fi
}

# Removes a secret
function delete_secret() {
    KEY="$1"
    gcloud secrets delete ${KEY} --quiet
}


function setup_bucket() {
    local BUCKET=${1-$BUCKET_NAME}
    local PROJECT=${2-$PROJECT_ID}
    gsutil ls -al gs://${BUCKET}/.dontremove 2> /dev/null
    if [[ $? > 0 ]]; then
        echo "Bucket does not exist, creating gs://${BUCKET}"
        gsutil mb -p ${PROJECT} gs://${BUCKET}
        if [[ $? > 0 ]]; then
            echo "Error: Cannot create bucket ${BUCKET} in ${PROJECT}"
            exit 1
        else
            gsutil cp .dontremove gs://${BUCKET}
        fi
    fi
}

function create_enable_service_account() {
    local SA_NAME="$1"
    local PROJECT="${2-$PROJECT_ID}"
    local DESCRIPTION="${3-Service account for GKE Hub}"
    local CURR_SA_NAME=$(gcloud iam service-accounts describe ${SA_NAME}@${PROJECT}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
    if [ -z "$CURR_SA_NAME" ]; then
        gcloud iam service-accounts create ${SA_NAME} --display-name "${DESCRIPTION}"
    else
        gcloud iam service-accounts enable "${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"
    fi
}

function disable_service_account() {
    local SA_NAME="$1"
    local PROJECT="${2-$PROJECT_ID}"
    local CURR_SA_NAME=$(gcloud iam service-accounts describe ${SA_NAME}@${PROJECT}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
    if [ ! -z "$CURR_SA_NAME" ]; then
        gcloud iam service-accounts disable "${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" -q
    fi
}

function remove_instance_in_zone() {
    INSTANCES=$1
    INSTANCE_ZONE=$2

    gcloud compute instances delete ${INSTANCES} --zone ${INSTANCE_ZONE} --delete-disks=all --quiet
}

function remove_all_instances() {
    # Iterate through zones
    export zone_count="${#AVAILABLE_ZONES[@]}"
    i=0
    for i in $(seq 1 $zone_count); do # 1-based seqence
        INSTANCE_ZONE="${AVAILABLE_ZONES[$i-1]}"
        INSTANCES=$(gcloud compute instances list --zones ${INSTANCE_ZONE} --filter="labels.type:k3s" --format="value(name)" | xargs | sed -e 's/ / /g')
        # Delete Instances in the zone
        if [[ ! -z "${INSTANCES}" ]]; then
            echo "Removing $INSTANCES in ${INSTANCE_ZONE}"
            remove_instance_in_zone "${INSTANCES}" "${INSTANCE_ZONE}"
        fi
    done
}

function get_login_token() {
    CLUSTER=$1

    CLUSTER_TOKEN_NAME="${CLUSTER}-k8s-token"

    FOUND_TOKEN=$(gcloud secrets versions access latest --secret="${CLUSTER_TOKEN_NAME}")

    echo ${FOUND_TOKEN}
}