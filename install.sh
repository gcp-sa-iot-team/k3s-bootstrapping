#!/bin/bash
unset K3S_COUNT
unset REGION_OVERRIDE
unset STARTING_INDEX

while getopts 'c:s:r:': option
do
    case "${option}" in
        c) K3S_COUNT=${OPTARG};;
        s) STARTING_INDEX="${OPTARG}";;
        r) REGION_OVERRIDE="${OPTARG}";;
    esac
done

usage()
{
  echo "Usage: ./install.sh   -c 1 (count)
                            [ -r us-west1 (region) ]
                            [ -s 1 (starting-index) ]"
  exit 1
}

if [[ -z "${K3S_COUNT}" || ${K3S_COUNT} -le 0 ]]; then
    echo "Missing count variable"
    usage
    exit 0
fi

# Load common variables & functions
source ./variables.sh

if [ "${GITLAB_SECRET_TOKEN}" == "PLEASE_SET" ]; then
    echo -e "\nERROR: GitLab Token is not set on environment variable GITLAB_SECRET_TOKEN.\nPlease set this variable either as an environment variable, or modify 'variables.sh' at the top of the file.\n"
    exit 1
fi

function create_service_account_secret() {
    local SA=$1
    local PROJECT=$2
    # Get service keys for new SA
    gcloud iam service-accounts keys create creds.json \
        --iam-account=${SA}@${PROJECT}.iam.gserviceaccount.com \
        --project=${PROJECT}

    # Add the key as a GCP secret
    create_secret "connect-sa" "creds.json" "true"
    # Remove generated creds after saving
    rm -rf creds.json
}

# Enable all services used by project
gcloud services enable \
    cloudresourcemanager.googleapis.com \
    gkeconnect.googleapis.com \
    compute.googleapis.com \
    gkehub.googleapis.com \
    secretmanager.googleapis.com

# Create service account for gke-hub
create_enable_service_account ${HUB_SA} ${PROJECT_ID}

# Give SA role to join environ
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${HUB_SA}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/gkehub.connect"

# Give SA role to join environ
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${HUB_SA}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/gkehub.admin"

# Create creds and store as secret
create_service_account_secret ${HUB_SA} ${PROJECT_ID}

# Create/Setup bucket
setup_bucket "${BUCKET_NAME}" "${PROJECT_ID}"
# Copy configuration
gsutil cp k3s-init.sh gs://${BUCKET_NAME}/k3s-init.sh

# Save bucket for use inside of each VM: NOTE: This is coupled to each VM's init script
create_secret "bucket-name" "${BUCKET_NAME}"

create_secret "root-repo" "${ROOT_REPO}"

create_secret "config-mgmt-version" "${CONFIG_SYNC_VERSION}"

# Create the GCP Secret for gitlab secret
create_secret "gitlab-acm-token" "${GITLAB_SECRET_TOKEN}"
create_secret "gitlab-acm-user" "${GITLAB_SECRET_USER}"

# Setup default compute service account with secrets accessor access
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com" \
    --role="roles/secretmanager.secretAccessor"

gcloud compute firewall-rules create allow-kubectl \
    --action allow \
    --target-tags kubectl \
    --source-ranges 0.0.0.0/0 \
    --rules tcp:6443

# count of available zones
export zone_count="${#AVAILABLE_ZONES[@]}"
echo "Distributing over $zone_count zones"

i=${CLUSTER_START_INDEX}
end=$((K3S_COUNT + CLUSTER_START_INDEX))
while [[ $i -lt ${end} ]]; do
    # chose one of the zones based on mod total count
    zoneIndex=$(($i%zone_count))
    INSTANCE_ZONE="${AVAILABLE_ZONES[$zoneIndex]}"

    echo "Starting cluster 'k3s-$i' into zone ${INSTANCE_ZONE}"

    gcloud compute instances create k3s-$i \
        --zone ${INSTANCE_ZONE} \
        --machine-type ${MACHINE_SIZE} \
        --tags http-server,kubectl \
        --preemptible \
        --description "GCE hosting k3s-${i}" \
        --labels type=k3s,role=k3s-master \
        --scopes=https://www.googleapis.com/auth/cloud-platform \
        --metadata "startup-script-url=gs://${BUCKET_NAME}/k3s-init.sh,k3s-version=${K3S_VERSION}" \
        --async
    let i=$i+1
done

echo "When all have completed, run: 'gsutil cp -r gs://${BUCKET_NAME}/kubeconfigs . ' to download all of the cluster config files"
echo "Then use 'export KUBECONFIG=<file>'" # Future, this can be included in .envrc files


echo ""
echo "---------------"
echo ""
echo "RUN: gcloud beta compute ssh --zone \"${ZONE}\" \"k3s-1\" --project \"${PROJECT_ID}\""
echo ""
echo "RUN: sudo journalctl -fu google-startup-scripts.service"
