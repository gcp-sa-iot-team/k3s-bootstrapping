#!/bin/bash
unset K3S_COUNT
unset REGION_OVERRIDE
unset STARTING_INDEX

while getopts 'r:': option
do
    case "${option}" in
        r) REGION_OVERRIDE="${OPTARG}";;
    esac
done

usage()
{
  echo "Usage: ./remove.sh  [ -r us-west1 (region) ]"
  exit 1
}

# Load common variables & functions
source ./variables.sh

# Disable service account
disable_service_account ${HUB_SA} ${PROJECT_ID}
# Delete key for GSA
GSA_FULL="${HUB_SA}@${PROJECT_ID}.iam.gserviceaccount.com"
KEYS=($(gcloud iam service-accounts keys list --iam-account="${GSA_FULL}" --filter="keyType=USER_MANAGED" --format="value(name)" | xargs | sed -e 's/ / /g'))
for KEY in "${KEYS[@]}"; do
    gcloud iam service-accounts keys delete "${KEY}" --iam-account="${GSA_FULL}" --quiet
done

# Get all instances with label "type=k3s" in available zones for region
export zone_count="${#AVAILABLE_ZONES[@]}"
i=0
for i in $(seq 1 $zone_count); do # 1-based seqence
    INSTANCE_ZONE="${AVAILABLE_ZONES[$i-1]}"

    INSTANCES=$(gcloud compute instances list --zones ${INSTANCE_ZONE} --filter="labels.type:k3s" --format="value(name)" | xargs | sed -e 's/ / /g')
    # Remove all instances (if there are instances)
    if [[ "$INSTANCES" != "" ]]; then
        # Unregister all clusters
        ARRAY=(${INSTANCES})
        for INSTANCE in "${ARRAY[@]}"; do
            gcloud container hub memberships delete ${INSTANCE} --quiet --async 2> /dev/null
        done
        # Remove all of the instances in the zone
        remove_instance_in_zone "${INSTANCES}" "${INSTANCE_ZONE}"
    fi
done

# Remove firewall
FIREWALL=$(gcloud compute firewall-rules describe allow-kubectl 2> /dev/null)
if [[ $? -eq 0 ]]; then
    gcloud compute firewall-rules delete allow-kubectl --quiet
fi

# Remove secrets
delete_secret "connect-sa"
delete_secret "bucket-name"
delete_secret "gitlab-acm-token"
delete_secret "gitlab-acm-user"
delete_secret "root-repo"

# Remove cluster configs
gsutil -m rm -r gs://$BUCKET_NAME/kubeconfigs
# Remove k3s init script
gsutil rm -r gs://$BUCKET_NAME/k3s-init.sh