#! /bin/bash

# Install k3s
K3S_VERSION=$(curl http://metadata/computeMetadata/v1/instance/attributes/k3s-version -H "Metadata-Flavor: Google")
if [ -z ${K3S_VERSION} ]; then
  curl -sfL https://get.k3s.io | sh -
else
  curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION="${K3S_VERSION}+k3s1" sh -
  if [ $? -gt 0 ]; then
    echo "Cannot download version ${K3S_VERSION} from releases"
    exit 1
  fi
fi

CLUSTER_NAME=$(curl http://metadata.google.internal/computeMetadata/v1/instance/name -H "Metadata-flavor: Google")

PROJECT_ID=$(gcloud config get-value core/project)
REGION=$(gcloud config get-value compute/region 2> /dev/null)
ZONE=$(gcloud config get-value compute/zone 2> /dev/null)

# Insert pub key for easy gcloud access
gcloud compute config-ssh
# Copy cluster config to bucket
BUCKET=$(gcloud secrets versions access latest --secret=bucket-name)
INTERNAL_IP=$(gcloud compute instances describe $CLUSTER_NAME --zone ${ZONE} --format='get(networkInterfaces[0].networkIP)')

cp /etc/rancher/k3s/k3s.yaml /etc/rancher/k3s/k3s-local-ip.yaml
chmod 444 /etc/rancher/k3s/k3s.yaml
sed -i "s/127.0.0.1/$INTERNAL_IP/g" /etc/rancher/k3s/k3s-local-ip.yaml
chmod 444 /etc/rancher/k3s/k3s-local-ip.yaml
echo "Copy KUBECONTEXT for '${CLUSTER_NAME}' to '${BUCKET}'"
# CommandException: "cp" command does not support provider-only URLs."
gsutil cp -r /etc/rancher/k3s/*.yaml gs://$BUCKET/kubeconfigs/$CLUSTER_NAME/

# Context for this script
KUBECONFIG="/etc/rancher/k3s/k3s.yaml"

# Give current user cluster-admin access (required for secret and SA creation)
kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole=cluster-admin \
  --user "admin"

gcloud secrets versions access latest --secret=connect-sa > creds.json

gcloud container hub memberships register ${CLUSTER_NAME} \
    --project=${PROJECT_ID} \
    --context=$(kubectl config current-context) \
    --kubeconfig=${KUBECONFIG} \
    --service-account-key-file="./creds.json"

# Remove creds after creating registration
rm -rf creds.json && true

# Create KSA for gke-hub
KSA_NAME=anthos-admin
kubectl create serviceaccount ${KSA_NAME}

kubectl create clusterrolebinding ${KSA_NAME} \
  --clusterrole cluster-admin --serviceaccount default:${KSA_NAME}

# Install ACM (Config Management base operator)
CONFIG_MGMT_VERSION="$(gcloud secrets versions access latest --secret=config-mgmt-version)"
gsutil cp gs://config-management-release/released/${CONFIG_MGMT_VERSION}/config-management-operator.yaml config-management-operator.yaml

kubectl apply -f config-management-operator.yaml

sleep 15s

# Wait for ConfigManagement & RootSync CRDs
kubectl wait --for=condition=established --timeout=60s crd configmanagements.configmanagement.gke.io


####################################################
####################################################
## Setup Config Management (Operator/Controller)

cat > config-management.yaml <<EOF
apiVersion: configmanagement.gke.io/v1
kind: ConfigManagement
metadata:
  name: config-management
spec:
  # clusterName is required and must be unique among all managed clusters
  clusterName: $CLUSTER_NAME
  enableMultiRepo: true
EOF

kubectl apply -f config-management.yaml

sleep 15s

kubectl wait --for=condition=established --timeout=120s crd rootsyncs.configsync.gke.io

## Configure Config Management (RootSync Operator)
SYNC_REPO="$(gcloud secrets versions access latest --secret=root-repo)"
GIT_CRED_SECRET="$(gcloud secrets versions access latest --secret="gitlab-acm-token")"
GIT_CRED_USER="$(gcloud secrets versions access latest --secret="gitlab-acm-user")"
ROOT_GIT_SECRET="git-creds-root"
# Create git-creds secret to view root sync repo
kubectl create secret generic ${ROOT_GIT_SECRET} \
  --namespace="config-management-system" \
  --from-literal="username=${GIT_CRED_USER}" \
  --from-literal=token="${GIT_CRED_SECRET}"

cat > root-sync-config.yaml <<EOF
apiVersion: configsync.gke.io/v1alpha1
kind: RootSync
metadata:
  name: root-sync
  namespace: config-management-system
spec:
  sourceFormat: hierarchy
  git:
    repo: "${SYNC_REPO}"
    branch: "main"
    dir: "config"
    auth: "token"
    secretRef:
      name: "${ROOT_GIT_SECRET}"

EOF

cat root-sync-config.yaml

kubectl apply -f root-sync-config.yaml

sleep 15s # these really annoy me

# Wait for the "root-reconsiler" deployment to become available
kubectl wait --for=condition=available --timeout=600s deployment root-reconciler -n config-management-system

echo -e "\n\nsource <(kubectl completion bash)\n" >> /etc/profile
echo -e "\nexport KUBECONFIG=${KUBECONFIG}\n" >> /etc/profile

export CLUSTER_NAME=$(curl http://metadata.google.internal/computeMetadata/v1/instance/name -H "Metadata-flavor: Google")
export SECRET_NAME=$(kubectl get serviceaccount console-cluster-reader -o jsonpath='{$.secrets[0].name}')
export KSA_TOKEN=$(kubectl get secret ${SECRET_NAME} -o jsonpath='{$.data.token}' | base64 --decode)

export TOKEN_NAME="${CLUSTER_NAME}-k8s-token"
gcloud secrets create $TOKEN_NAME --replication-policy="automatic" || true # doesn't matter if it exists or not
echo ${KSA_TOKEN} | gcloud secrets versions add ${TOKEN_NAME} --data-file=-

echo "Done!!!"